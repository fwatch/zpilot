/*
 * Copyright (c) 2021 zpilot.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/sys/printk.h>

#ifdef CONFIG_EVENTS

#define EVT_500HZ BIT(0)
#define EVT_200HZ BIT(1)
#define EVT_100HZ BIT(2)
#define EVT_50HZ BIT(3)
#define EVT_10HZ BIT(4)
#define EVT_1HZ BIT(5)

static K_EVENT_DEFINE(worker_event);
#endif

static const struct gpio_dt_spec led = GPIO_DT_SPEC_GET(DT_ALIAS(led0), gpios);

void main(void)
{
	uint32_t events = 0;
	int ret = 0;

	printk("Hello World! %s\n", CONFIG_BOARD);
	if (!device_is_ready(led.port))
	{
		return;
	}
	ret = gpio_pin_configure_dt(&led, GPIO_OUTPUT_ACTIVE);
	if (ret < 0)
	{
		return;
	}
	while (1)
	{
		k_msleep(500);

		gpio_pin_toggle_dt(&led);
	}

	return ret;
}
