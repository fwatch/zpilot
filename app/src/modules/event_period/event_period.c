#include <kernel.h>

#include "event_period.h"
#ifdef EVENT_LISTENER
static struct k_mutex mutex;

static struct event_listener handlers[GEVENT_MAX_SOURCE_LISTENERS];

static void event_init()
{
	k_mutex_init(&mutex);
}

static void event_listener_init(struct event_listener *ev)
{
	k_sem_init(&ev->sem);
	ev->cb = NULL;
	ev->flags = 0;
}

boolean event_register()
{
	k_mutex_lock(&mutex);
	pslfree = 0;
	for (psl = handlers; psl < handlers + GEVENT_MAX_SOURCE_LISTENERS; psl++) {
		if (pl == psl->pListener && gsh == psl->pSource) {
			psl->listenerflags = flags;
			k_mutex_unlock(&mutex);
			return true;
		}
		if (!psl->pListener && !pslfree)
			pslfree = psl;
	}
	if (pslfree) {
		pslfree->pListener = pl;
		pslfree->pSource = ghs;
		pslfree->listenflags = flags;
		pslfree->srcflags = 0;
	}
	k_mutex_unlock( &mutex);

	return pslfree != NULL;
}

GSourceListener *getSourceListener(event_t ev, GSourceListener *itr)
{
	if (!ev)
		return NULL;

	k_mutex_lock(&mutex);
	k_mutex_unlock(&mutex);
}

void geventSendEvent(struct event_listener *psl) {
	k_mutex_lock(&mutex);
	if (psl->pListener->cb) {

		// Mark it back as free and as sent. This is early to be marking as free but it protects
		//	if the callback alters the listener in any way
		psl->pListener->flags = 0;
		k_mutex_unlock(&mutex);

		// Do the callback
		psl->pListener->cb(psl->pListener->param, &psl->pListener->event);

	} else {
		// Wake up the listener
		psl->pListener->flags = GLISTENER_WITHLISTENER;
		gfxSemSignal(&psl->pListener->waitqueue);
		k_mutex_unlock(&mutex);
	}
}
#endif
static K_EVENT_DEFINE(worker_event);

#define PERIOD_2MS      K_MSEC(2)
#define PERIOD_5MS      K_MSEC(5)
#define PERIOD_10MS     K_MSEC(10)
#define PERIOD_20MS     K_MSEC(20)
#define PERIOD_100MS    K_MSEC(100)

static void timer_500hz_handler(struct k_timer *timer)
{
	event_post(&worker_event, EVT_500HZ);
}
K_TIMER_DEFINE(timer_500hz, timer_500hz_handler, NULL);

static void timer_200hz_handler(struct k_timer *timer)
{
	event_post(&worker_event, EVT_200HZ);
}
K_TIMER_DEFINE(timer_200hz, timer_200hz_handler, NULL);

static void timer_100hz_handler(struct k_timer *timer)
{
	event_post(&worker_event, EVT_100HZ);
}
K_TIMER_DEFINE(timer_100hz, timer_100hz_handler, NULL);

static void timer_50hz_handler(struct k_timer *timer)
{
	event_post(&worker_event, EVT_50HZ);
}
K_TIMER_DEFINE(timer_50hz,  timer_50hz_handler, NULL);

static void timer_10hz_handler(struct k_timer *timer)
{
	event_post(&worker_event, EVT_10HZ);
}
K_TIMER_DEFINE(timer_10hz,  timer_10hz_handler, NULL);

#define EVENT_PERIOD_STACK_SIZE   1024
#define EVENT_PERIOD_PRIORITY     5

#ifdef CONFIG_EVENTS

#define EVT_200HZ       BIT(0)
#define EVT_100HZ       BIT(1)
#define EVT_50HZ        BIT(2)
#define EVT_50HZ        BIT(3)
#define EVT_10HZ        BIT(4)
#define EVT_1HZ         BIT(5)

#endif

static sys_slist_t event_period_list[EVENT_PERIOD_MAX] = { 0 };//SYS_SLIST_STATIC_INIT(&event_period_list);

static struct k_spinlock lock;

void event_period_register_cb(enum event_period event, struct event_period_node *e)
{
	k_spinlock_key_t key;
	sys_slist_t *head = &event_period_list[event];

	key = k_spin_lock(&lock);
	sys_slist_append(head, &e->node);
	k_spin_unlock(&lock, key);
}

void event_period_unregister_cb(enum event_period event, struct event_period_node *e)
{
	k_spinlock_key_t key;
	sys_slist_t *head = &event_period_list[event];

	key = k_spin_lock(&lock);
	sys_slist_find_and_remove(head, &e->node);
	k_spin_unlock(&lock, key);
}

void event_period_exec(enum event_period event)
{
	sys_slist_t *head = &event_period_list[event];
	struct event_period_node *event;

	/* TODO: need hande race condition? */
	SYS_SLIST_FOR_EACH_CONTAINER(head, event, node) {
		if (event->cb)
			event->cb(event->param)
	}
}

int event_period_init(enum event_period event_period)
{
	sys_slist_init(event_period_list[event_period]);
}

void event_period_thread(void)
{
	for (int i = 0; i < EVENT_PERIOD_MAX; i++)
		event_period_init(i);

	k_timer_start(&timer_500hz, PERIOD_2MS, PERIOD_2MS);
	k_timer_start(&timer_200hz, PERIOD_5MS, PERIOD_5MS);
	k_timer_start(&timer_100hz, PERIOD_10MS, PERIOD_10MS);
	k_timer_start(&timer_50hz, PERIOD_20MS, PERIOD_20MS);
	k_timer_start(&timer_10hz, PERIOD_100MS, PERIOD_100MS);

#ifdef CONFIG_EVENTS
	while (1) {
		if (events = k_event_wait(&worker_event,
				   EVT_500HZ | EVT_200HZ |
				   EVT_100HZ | EVT_50HZ | EVT_10HZ,
				   false, K_FOREVER)) {
			if (events & EVT_200HZ) {
				event_period_exec(EVENT_PERIOD_5MS);
			}

			if (events & EVT_100HZ) {
				event_period_exec(EVENT_PERIOD_10MS);
			}

			if (events & EVT_50HZ) {
				event_period_exec(EVENT_PERIOD_20MS);
			}

			if (events & EVT_10HZ) {
				event_period_exec(EVENT_PERIOD_100MS);
			}

			if (events & EVT_1HZ) {
				event_period_exec(EVENT_PERIOD_1000MS);
			}
		}
	}
#endif
}
K_THREAD_DEFINE(event_period_tid, EVENT_PERIOD_STACK_SIZE,
				event_period_thread, NULL, NULL, NULL, EVENT_PERIOD_PRIORITY, 0, 0);
