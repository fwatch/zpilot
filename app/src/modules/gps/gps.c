#include <device.h>
#include <drivers/uart.h>
#include <logging/log.h>
#include <smf.h>

#define GPS_COM_DEV "UART_3"

#define GPS_STACK_SIZE   1024
#define GPS_PRIORITY     5

LOG_MODULE_REGISTER(gps, LOG_LEVEL_DBG);

K_MSGQ_DEFINE(gps_uart_msgq, sizeof(uint8_t), 32, 4);

enum gps_status {
	GPS_STATE_START,
	GPS_STATE_WAITHEAD,
	GPS_STATE_RECVMSG,
	GPS_STATE_END
};

struct gps_ctx {
	struct smf_ctx ctx;
	bool start;
};

struct gps_ctx gps_ctx = { 0 };

static const struct device *uart_dev = NULL;

static void gps_state_start_run(void *arg);
static void gps_state_recv_run(void *arg);

static int gps_get_char(uint8_t *c)
{
	return k_msgq_get(&gps_uart_msgq, c, K_FOREVER);
}

static const struct smf_state gps_states[] = {
        [GPS_STATE_START] = SMF_CREATE_STATE(NULL, gps_state_start_run, NULL),
        [GPS_STATE_RECVMSG] = SMF_CREATE_STATE(NULL, gps_state_recv_run, NULL),
};

static void gps_state_start_run(void *arg)
{
	struct gps_cts *ctx = arg;
	uint8_t c = 0;

	gps_get_char(&c);
	if ((ctx->start_flag == false) && (c == '$')) {
        ctx->start_flag = true;
        ctx->found_cr = false;
        ctx->tx_counter = 0;
		smf_set_state(SMF_CTX(&gps_ctx), &gps_states[GPS_STATE_RECVMSG]);
	}
}

static void gps_state_recv_run(void *arg)
{
	struct gps_cts *ctx = arg;
	uint8_t c = 0;

	gps_get_char(&c);

	ctx->gps_rx_buffer[ctx->rx_counter++] = c;
	if (!ctx->found_cr && c == '\n') {
		ctx->found_cr = true;
	}
}

static void uart_rx_isr(const struct device *dev, void *user_data)
{
//	const struct device *dev = user_data;

	if (uart_irq_update(dev) {
		if (uart_irq_rx_ready(dev)) {
			uint8_t c;
			uart_fifo_read(dev, &c, 1);

			while (k_msgq_put(&gps_uart_msgq, &c, K_NO_WAIT) != 0) {
				k_msgq_purge(&gps_uart_msgq);
			}
		} else if (uart_irq_tx_ready(dev)) {
		uart_fifo_fill(dev, ch, 1);
	}
	}
}

int gps_init(const struct device *dev)
{
	uart_dev = device_get_binding(MAVLINK_COM_DEV);
	if (!uart_dev) {
		LOG_ERR("can not open uart device: %s", MAVLINK_COM_DEV);
		return -ENODEV;
	}
	/* setup serial */
	uart_irq_rx_disable(uart_dev);
	//uart_irq_tx_disable(uart_dev);
	// flush();
	uart_irq_callback_user_data_set(uart_dev, uart_rx_isr, NULL);
	uart_irq_rx_enable(uart_dev);

	return 0;
}
SYS_INIT(gps_init, APPLICATION, CONFIG_KERNEL_INIT_PRIORITY_DEFAULT);

void gps_thread(void)
{
	int retval = 0;

	smf_set_initial(SMF_CTX(&gps_ctx), &gps_states[GPS_STATE_START]);

	while (1) {
		smf_run_state(SMF_CTX(&gps_ctx));
	}
}
K_THREAD_DEFINE(gps_tid, GPS_STACK_SIZE,
				gps_thread, NULL, NULL, NULL, GPS_PRIORITY, 0, 0);