/*
 * Copyright (c) 2024 zpilot.
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/sensor.h>
#include <zephyr/logging/log.h>

#include <zephyr/zbus/zbus.h>

#define SENSOR_STACK_SIZE   2048
#define SENSOR_PRIORITY     5

#define LOG_LEVEL LOG_LEVEL_INF
LOG_MODULE_REGISTER(sensor);

struct accel_msg {
	struct sensor_value x;
	struct sensor_value y;
	struct sensor_value z;
	struct sensor_value gx;
	struct sensor_value gy;
	struct sensor_value gz;
};

ZBUS_CHAN_DEFINE(accel_chan,       /* Name */
		 struct accel_msg, /* Message type */

		 NULL,                 /* Validator */
		 NULL,                 /* User data */
		 ZBUS_OBSERVERS(accel_sub), /* observers */
		 ZBUS_MSG_INIT(0) /* Initial value major 0, minor 1, build 2 */
);

#ifdef CONFIG_MPU6000_TRIGGER
static void mpu6000_trigger_handler(const struct device *dev,
				    const struct sensor_trigger *trig)
{
	static struct sensor_value accel_x, accel_y, accel_z;
	static struct sensor_value gyro_x, gyro_y, gyro_z;

	sensor_sample_fetch_chan(dev, SENSOR_CHAN_ALL);
	sensor_channel_get(dev, SENSOR_CHAN_ACCEL_X, &accel_x);
	sensor_channel_get(dev, SENSOR_CHAN_ACCEL_Y, &accel_y);
	sensor_channel_get(dev, SENSOR_CHAN_ACCEL_Z, &accel_z);

	sensor_channel_get(dev, SENSOR_CHAN_GYRO_X, &gyro_x);
	sensor_channel_get(dev, SENSOR_CHAN_GYRO_Y, &gyro_y);
	sensor_channel_get(dev, SENSOR_CHAN_GYRO_Z, &gyro_z);
	LOG_DBG("fetch sample %f %f %f", sensor_value_to_double(&accel_x),
			sensor_value_to_double(&accel_y), sensor_value_to_double(&accel_z));
	// TODO: zbus publish
	// accel_raw gyro_raw
	struct accel_msg msg = { 0 };
	msg.x = accel_x;
	msg.y = accel_y;
	msg.z = accel_z;
	msg.gx = gyro_x;
	msg.gy = gyro_y;
	msg.gz = gyro_z;
	zbus_chan_pub(&accel_chan, &msg, K_NO_WAIT);
}
#endif

void sensor_init()
{
#ifdef CONFIG_BOARD_MOON
#endif

#ifdef CONFIG_BOARD_CC3D
	const struct device *mpu6000_dev = DEVICE_DT_GET_ONE(inv_mpu6000);
	if (!device_is_ready(mpu6000_dev)) {
		LOG_ERR("failed to open device MPU6000");
		return;
	}
	struct sensor_trigger trig;

	trig.type = SENSOR_TRIG_DATA_READY;
	trig.chan = SENSOR_CHAN_ALL;

	if (sensor_trigger_set(mpu6000_dev, &trig, mpu6000_trigger_handler) != 0) {
		printk("Could not set sensor type and channel\n");
		return 0;
	}
#endif
}
SYS_INIT(sensor_init, APPLICATION, 0);


ZBUS_SUBSCRIBER_DEFINE(accel_sub, 4);

#include "Fusion.h"

void sensor_thread(void)
{
FusionAhrs ahrs;
FusionAhrsInitialise(&ahrs);
	const struct zbus_channel *chan;
	while (1) {
		if (!zbus_sub_wait(&accel_sub, &chan, K_FOREVER)) {
			struct accel_msg data;

			zbus_chan_read(&accel_chan, &data, K_FOREVER);
			//LOG_DBG("%f %f %f %f %f %f", 	sensor_value_to_double(&data.x), 	sensor_value_to_double(&data.y), 	sensor_value_to_double(&data.z),
			//				sensor_value_to_double(&data.gx),       sensor_value_to_double(&data.gy),       sensor_value_to_double(&data.gz));
			//LOG_INF("%f %f %f",     sensor_value_to_double(&data.gx),       sensor_value_to_double(&data.gy),       sensor_value_to_double(&data.gz));
			double ax = sensor_value_to_double(&data.x);
			double ay = sensor_value_to_double(&data.y);
			double az = sensor_value_to_double(&data.z);
			double gx = sensor_value_to_double(&data.gx);
			double gy = sensor_value_to_double(&data.gy);
			double gz = sensor_value_to_double(&data.gz);

const FusionVector gyroscope = { gx, gy, gz }; // replace this with actual gyroscope data in degrees/s
const FusionVector accelerometer = { ax, ay, az }; // replace this with actual accelerometer data in g

FusionAhrsUpdateNoMagnetometer(&ahrs, gyroscope, accelerometer, 0.01f);
#if 1
const FusionEuler euler = FusionQuaternionToEuler(FusionAhrsGetQuaternion(&ahrs));
#endif

printf("Roll %0.1f, Pitch %0.1f, Yaw %0.1f\n", euler.angle.roll, euler.angle.pitch, euler.angle.yaw);
			//const
		}
	}
}
K_THREAD_DEFINE(sensor_tid, SENSOR_STACK_SIZE, sensor_thread, NULL, NULL, NULL, SENSOR_PRIORITY, 0, 0);
