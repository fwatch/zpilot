#

zephyr_library()

zephyr_library_include_directories(Fusion)

zephyr_library_sources(Fusion/FusionAhrs.c)
zephyr_library_sources(Fusion/FusionCompass.c)
zephyr_library_sources(Fusion/FusionOffset.c)

zephyr_include_directories(Fusion)