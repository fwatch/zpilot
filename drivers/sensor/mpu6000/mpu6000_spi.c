#define DT_DRV_COMPAT inv_mpu6000

#include "mpu6000.h"

static int mpu6000_raw_read(const struct device *dev, uint8_t reg_addr,
			    uint8_t *value, uint8_t len)
{
	struct mpu6000_data *data = dev->data;
	const struct mpu6000_config *cfg = dev->config;
	const struct spi_config *spi_cfg = &cfg->bus_cfg.spi_cfg->spi_conf;
	uint8_t buffer_tx[2] = { reg_addr | mpu6000_SPI_READ, 0 };
	const struct spi_buf tx_buf = {
			.buf = buffer_tx,
			.len = 2,
	};
	const struct spi_buf_set tx = {
		.buffers = &tx_buf,
		.count = 1
	};
	const struct spi_buf rx_buf[2] = {
		{
			.buf = NULL,
			.len = 1,
		},
		{
			.buf = value,
			.len = len,
		}
	};
	const struct spi_buf_set rx = {
		.buffers = rx_buf,
		.count = 2
	};


	if (len > 64) {
		return -EIO;
	}
	if (spi_transceive_dt(&cfg->spi, spi_cfg, &tx, &rx)) {
	//if (spi_transceive(data->bus, spi_cfg, &tx, &rx)) {
		return -EIO;
	}

	return 0;
}

static int mpu600_raw_write(const struct device *dev, uint8_t reg_addr,
			     uint8_t *value, uint8_t len)
{
	struct mpu6000_data *data = dev->data;
	const struct mpu6000_config *cfg = dev->config;
	const struct spi_config *spi_cfg = &cfg->bus_cfg.spi_cfg->spi_conf;
	uint8_t buffer_tx[1] = { reg_addr & ~mpu6000_SPI_READ };
	const struct spi_buf tx_buf[2] = {
		{
			.buf = buffer_tx,
			.len = 1,
		},
		{
			.buf = value,
			.len = len,
		}
	};
	const struct spi_buf_set tx = {
		.buffers = tx_buf,
		.count = 2
	};


	if (len > 64) {
		return -EIO;
	}

	if (spi_write_dt(&cfg->spi, spi_cfg, &tx)) {
		return -EIO;
	}

	return 0;
}

static int mpu6000_spi_read_data(const struct device *dev, uint8_t reg_addr,
				 uint8_t *value, uint8_t len)
{
	return mpu6000_raw_read(dev, reg_addr, value, len);
}

static int mpu6000_spi_write_data(const struct device *dev, uint8_t reg_addr,
				  uint8_t *value, uint8_t len)
{
	return mpu6000_raw_write(dev, reg_addr, value, len);
}

static int mpu6000_spi_read_reg(const struct device *dev, uint8_t reg_addr,
				uint8_t *value)
{
	return mpu6000_raw_read(dev, reg_addr, value, 1);
}

static int mpu6000_spi_update_reg(const struct device *dev, uint8_t reg_addr,
				  uint8_t mask, uint8_t value)
{
	uint8_t tmp_val;

	mpu6000_raw_read(dev, reg_addr, &tmp_val, 1);
	tmp_val = (tmp_val & ~mask) | (value & mask);

	return mpu6000_raw_write(dev, reg_addr, &tmp_val, 1);
}

static const struct mpu6000_transfer_function mpu6000_spi_transfer_fn = {
	.read_data = mpu6000_spi_read_data,
	.write_data = mpu6000_spi_write_data,
	.read_reg  = mpu6000_spi_read_reg,
	.update_reg = mpu6000_spi_update_reg,
};

int mpu6000_spi_init(const struct device *dev)
{
	struct mpu6000_data *data = dev->data;
	const struct mpu6000_config *cfg = dev->config;
	const struct mpu6000_spi_cfg *spi_cfg = cfg->bus_cfg.spi_cfg;

	data->hw_tf = &mpu6000_spi_transfer_fn;

	if (spi_cfg->cs_gpios_label != NULL) {

		/* handle SPI CS thru GPIO if it is the case */
		data->cs_ctrl.gpio_dev =
			    device_get_binding(spi_cfg->cs_gpios_label);
		if (!data->cs_ctrl.gpio_dev) {
			LOG_ERR("Unable to get GPIO SPI CS device");
			return -ENODEV;
		}

		LOG_DBG("SPI GPIO CS configured on %s:%u",
			spi_cfg->cs_gpios_label, data->cs_ctrl.gpio_pin);
	}

	return 0;
}