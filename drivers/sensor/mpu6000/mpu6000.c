/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT inv_mpu6000

#include <zephyr/drivers/spi.h>
#include <zephyr/init.h>
#include <zephyr/sys/byteorder.h>
#include <zephyr/drivers/sensor.h>
#include <zephyr/logging/log.h>

#include "mpu6000.h"

LOG_MODULE_REGISTER(MPU6000, CONFIG_SENSOR_LOG_LEVEL);

/* see "Accelerometer Measurements" section from register map description */
static void mpu6000_convert_accel(struct sensor_value *val, int16_t raw_val,
				  uint16_t sensitivity_shift)
{
	int64_t conv_val;

	conv_val = ((int64_t)raw_val * SENSOR_G) >> sensitivity_shift;
	val->val1 = conv_val / 1000000;
	val->val2 = conv_val % 1000000;
}

/* see "Gyroscope Measurements" section from register map description */
static void mpu6000_convert_gyro(struct sensor_value *val, int16_t raw_val,
				 uint16_t sensitivity_x10)
{
	int64_t conv_val;

	conv_val = ((int64_t)raw_val * SENSOR_PI * 10) /
		   (sensitivity_x10 * 180U);
	val->val1 = conv_val / 1000000;
	val->val2 = conv_val % 1000000;
}

/* see "Temperature Measurement" section from register map description */
static inline void mpu6000_convert_temp(struct sensor_value *val,
					int16_t raw_val)
{
	val->val1 = raw_val / 340 + 36;
	val->val2 = ((int64_t)(raw_val % 340) * 1000000) / 340 + 530000;

	if (val->val2 < 0) {
		val->val1--;
		val->val2 += 1000000;
	} else if (val->val2 >= 1000000) {
		val->val1++;
		val->val2 -= 1000000;
	}
}

static int mpu6000_channel_get(const struct device *dev,
			       enum sensor_channel chan,
			       struct sensor_value *val)
{
	struct mpu6000_data *drv_data = dev->data;

	switch (chan) {
	case SENSOR_CHAN_ACCEL_XYZ:
		mpu6000_convert_accel(val, drv_data->accel_x,
				      drv_data->accel_sensitivity_shift);
		mpu6000_convert_accel(val + 1, drv_data->accel_y,
				      drv_data->accel_sensitivity_shift);
		mpu6000_convert_accel(val + 2, drv_data->accel_z,
				      drv_data->accel_sensitivity_shift);
		break;
	case SENSOR_CHAN_ACCEL_X:
		mpu6000_convert_accel(val, drv_data->accel_x,
				      drv_data->accel_sensitivity_shift);
		break;
	case SENSOR_CHAN_ACCEL_Y:
		mpu6000_convert_accel(val, drv_data->accel_y,
				      drv_data->accel_sensitivity_shift);
		break;
	case SENSOR_CHAN_ACCEL_Z:
		mpu6000_convert_accel(val, drv_data->accel_z,
				      drv_data->accel_sensitivity_shift);
		break;
	case SENSOR_CHAN_GYRO_XYZ:
		mpu6000_convert_gyro(val, drv_data->gyro_x,
				     drv_data->gyro_sensitivity_x10);
		mpu6000_convert_gyro(val + 1, drv_data->gyro_y,
				     drv_data->gyro_sensitivity_x10);
		mpu6000_convert_gyro(val + 2, drv_data->gyro_z,
				     drv_data->gyro_sensitivity_x10);
		break;
	case SENSOR_CHAN_GYRO_X:
		mpu6000_convert_gyro(val, drv_data->gyro_x,
				     drv_data->gyro_sensitivity_x10);
		break;
	case SENSOR_CHAN_GYRO_Y:
		mpu6000_convert_gyro(val, drv_data->gyro_y,
				     drv_data->gyro_sensitivity_x10);
		break;
	case SENSOR_CHAN_GYRO_Z:
		mpu6000_convert_gyro(val, drv_data->gyro_z,
				     drv_data->gyro_sensitivity_x10);
		break;
	case SENSOR_CHAN_DIE_TEMP:
		mpu6000_convert_temp(val, drv_data->temp);
	default:
		return -ENOTSUP;
	}

	return 0;
}

static int mpu6000_raw_read(const struct device *dev, uint8_t reg_addr,
			    uint8_t *value, uint8_t len)
{
	struct mpu6000_data *data = dev->data;
	const struct mpu6000_config *cfg = dev->config;
	//const struct spi_config *spi_cfg = &cfg->bus_cfg.spi_cfg->spi_conf;
	uint8_t buffer_tx[1] = { reg_addr | MPU6000_SPI_READ };
	const struct spi_buf tx_buf = {
			.buf = buffer_tx,
			.len = 1,
	};
	const struct spi_buf_set tx = {
		.buffers = &tx_buf,
		.count = 1
	};
	const struct spi_buf rx_buf[2] = {
		{
			.buf = NULL,
			.len = 1,
		},
		{
			.buf = value,
			.len = len,
		}
	};
	const struct spi_buf_set rx = {
		.buffers = rx_buf,
		.count = 2
	};


	if (len > 64) {
		return -EIO;
	}
	if (spi_transceive_dt(&cfg->spi, &tx, &rx)) {
	//if (spi_transceive(data->bus, spi_cfg, &tx, &rx)) {
		return -EIO;
	}

	return 0;
}

static int mpu6000_raw_write(const struct device *dev, uint8_t reg_addr,
			     uint8_t *value, uint8_t len)
{
	struct mpu6000_data *data = dev->data;
	const struct mpu6000_config *cfg = dev->config;
	//const struct spi_config *spi_cfg = &cfg->bus_cfg.spi_cfg->spi_conf;
	uint8_t buffer_tx[1] = { reg_addr & ~MPU6000_SPI_READ };
	const struct spi_buf tx_buf[2] = {
		{
			.buf = buffer_tx,
			.len = 1,
		},
		{
			.buf = value,
			.len = len,
		}
	};
	const struct spi_buf_set tx = {
		.buffers = tx_buf,
		.count = 2
	};


	if (len > 64) {
		return -EIO;
	}

	if (spi_write_dt(&cfg->spi, &tx)) {
		return -EIO;
	}

	return 0;
}

static int mpu6000_spi_read_data(const struct device *dev, uint8_t reg_addr,
				 uint8_t *value, uint8_t len)
{
	return mpu6000_raw_read(dev, reg_addr, value, len);
}

static int mpu6000_spi_write_data(const struct device *dev, uint8_t reg_addr,
				  uint8_t *value, uint8_t len)
{
	return mpu6000_raw_write(dev, reg_addr, value, len);
}

static int mpu6000_spi_read_reg(const struct device *dev, uint8_t reg_addr,
				uint8_t *value)
{
	return mpu6000_raw_read(dev, reg_addr, value, 1);
}

static int mpu6000_spi_update_reg(const struct device *dev, uint8_t reg_addr,
				  uint8_t mask, uint8_t value)
{
	uint8_t tmp_val;

	mpu6000_raw_read(dev, reg_addr, &tmp_val, 1);
	tmp_val = (tmp_val & ~mask) | (value & mask);

	return mpu6000_raw_write(dev, reg_addr, &tmp_val, 1);
}

static int mpu6000_sample_fetch(const struct device *dev,
				enum sensor_channel chan)
{
	struct mpu6000_data *drv_data = dev->data;
	const struct mpu6000_config *cfg = dev->config;
	uint8_t int_status;
	int16_t buf[7];
	int ret = 0;

	ret = mpu6000_spi_read_data(dev, MPU6000_REG_INT_STATUS, &int_status, 1);
	if (ret < 0) {
		LOG_ERR("Failed to read data sample!");
		return -EIO;
	}

	ret = mpu6000_spi_read_data(dev, MPU6000_REG_DATA_START, (uint8_t *)buf, sizeof(buf));
	if (ret < 0) {
		LOG_ERR("Failed to read data sample!");
		return -EIO;
	}
	drv_data->accel_x = sys_be16_to_cpu(buf[0]);
	drv_data->accel_y = sys_be16_to_cpu(buf[1]);
	drv_data->accel_z = sys_be16_to_cpu(buf[2]);
	drv_data->temp = sys_be16_to_cpu(buf[3]);
	drv_data->gyro_x = sys_be16_to_cpu(buf[4]);
	drv_data->gyro_y = sys_be16_to_cpu(buf[5]);
	drv_data->gyro_z = sys_be16_to_cpu(buf[6]);

	return ret;
}

static const struct sensor_driver_api mpu6000_driver_api = {
#if CONFIG_MPU6000_TRIGGER
	.trigger_set = mpu6000_trigger_set,
#endif
	.sample_fetch = mpu6000_sample_fetch,
	.channel_get = mpu6000_channel_get,
};

int mpu6000_init(const struct device *dev)
{
	struct mpu6000_data *drv_data = dev->data;
	const struct mpu6000_config *cfg = dev->config;
	uint8_t id, i;
	uint8_t data;
	int ret = 0;
	if (!device_is_ready(cfg->spi.bus)) {
		LOG_ERR("Bus device is not ready");
		return -ENODEV;
	}
	k_busy_wait(100000);
	/* check chip ID */
	if (mpu6000_raw_read(dev, MPU6000_REG_CHIP_ID, &id, 1) < 0) {
		LOG_ERR("Failed to read chip ID.");
		return -EIO;
	}

	if (id != MPU6000_CHIP_ID) {
		LOG_ERR("Invalid chip ID:0x%x", id);
		return -EINVAL;
	}
	/* wake up chip */
	data = 0;
	mpu6000_spi_write_data(dev, MPU6000_REG_PWR_MGMT1, &data, 1);
	k_busy_wait(100000);
#if 0
	if (i2c_reg_update_byte_dt(&cfg->i2c, MPU6000_REG_PWR_MGMT1,
				   MPU6000_SLEEP_EN, 0) < 0) {
		LOG_ERR("Failed to wake up chip.");
		return -EIO;
	}
#endif


	/* set accelerometer full-scale range */
	for (i = 0U; i < 4; i++) {
		if (BIT(i+1) == CONFIG_MPU6000_ACCEL_FS) {
			break;
		}
	}

	if (i == 4U) {
		LOG_ERR("Invalid value for accel full-scale range.");
		return -EINVAL;
	}

	//if (i2c_reg_write_byte_dt(&cfg->i2c, MPU6000_REG_ACCEL_CFG,
	data = i << MPU6000_ACCEL_FS_SHIFT;
	if (mpu6000_spi_write_data(dev, MPU6000_REG_ACCEL_CFG, &data, 1) < 0) {
				  //i << MPU6000_ACCEL_FS_SHIFT) < 0) {
		LOG_ERR("Failed to write accel full-scale range.");
		return -EIO;
	}

	drv_data->accel_sensitivity_shift = 14 - i;

	/* set gyroscope full-scale range */
	for (i = 0U; i < 4; i++) {
		if (BIT(i) * 250 == CONFIG_MPU6000_GYRO_FS) {
			break;
		}
	}

	if (i == 4U) {
		LOG_ERR("Invalid value for gyro full-scale range.");
		return -EINVAL;
	}

	//if (i2c_reg_write_byte_dt(&cfg->i2c, MPU6000_REG_GYRO_CFG,
				  //i << MPU6000_GYRO_FS_SHIFT) < 0) {
	data = i << MPU6000_GYRO_FS_SHIFT;
	if (mpu6000_spi_write_data(dev, MPU6000_REG_GYRO_CFG, &data, 1) < 0) {
		LOG_ERR("Failed to write gyro full-scale range.");
		return -EIO;
	}

	drv_data->gyro_sensitivity_x10 = mpu6000_gyro_sensitivity_x10[i];

#ifdef CONFIG_MPU6000_TRIGGER
	/*data = INT_LEVEL;
	ret = mpu6000_spi_write_data(dev,MPU6000_REG_INT_PIN_CFG, &data, 1);
	if (ret < 0) {
		LOG_ERR("failed to set intterput pin!");
	}*/
	data = MPU6000_DRDY_EN;
	ret = mpu6000_spi_write_data(dev, MPU6000_REG_INT_ENABLE, &data, 1);
	if (ret < 0) {
		LOG_ERR("failed to set intterput!");
	}
	if (cfg->int_gpio.port) {
		if (mpu6000_init_interrupt(dev) < 0) {
			LOG_DBG("Failed to initialize interrupts.");
			return -EIO;
		}
	}
#endif

#if 1

	mpu6000_raw_read(dev, MPU6000_REG_PWR_MGMT1, &data, 1);
	LOG_INF("MPU6000_REG_PWR_MGMT1	0x%x", data);
	mpu6000_raw_read(dev, MPU6000_REG_INT_PIN_CFG, &data, 1);
	LOG_INF("MPU6000_REG_INT_PIN_CFG	0x%x", data);
	mpu6000_raw_read(dev, MPU6000_REG_INT_ENABLE, &data, 1);
	LOG_INF("MPU6000_REG_INT_ENABLE		0x%x", data);
#endif
	return 0;
}

#define MPU6000_DEFINE(inst)									\
	static struct mpu6000_data mpu6000_data_##inst;						\
												\
	static const struct mpu6000_config mpu6000_config_##inst = {				\
		.spi = SPI_DT_SPEC_INST_GET(inst, SPI_OP_MODE_MASTER | SPI_MODE_CPOL |          \
		                           SPI_MODE_CPHA | SPI_WORD_SET(8), 0),			\
		IF_ENABLED(CONFIG_MPU6000_TRIGGER,						\
			   (.int_gpio = GPIO_DT_SPEC_INST_GET_OR(inst, int_gpios, { 0 }),))	\
	};											\
												\
	SENSOR_DEVICE_DT_INST_DEFINE(inst, mpu6000_init, NULL,					\
			      &mpu6000_data_##inst, &mpu6000_config_##inst,			\
			      POST_KERNEL, CONFIG_SENSOR_INIT_PRIORITY,				\
			      &mpu6000_driver_api);						\

DT_INST_FOREACH_STATUS_OKAY(MPU6000_DEFINE)
