### Overview

zephyr based pilot and UAVs

### Initialization
For developer
```
 west init -m git@gitee.com:fwatch/zpilot.git --mr develop zpilot

cd zpilot && west update
```

### Building and flash
```
cd zpilot/zpilot/app

west build -p auto -b <board> .

west flash
```

### support board list
moon(legacy)

kite(legacy)

cc3d

naze32

acro

deluxe

F3-v3-pro

revo

revo_mini
